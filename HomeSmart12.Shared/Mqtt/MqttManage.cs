﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Net.Mqtt;
using System.Linq;
using HomeSmart12.Entites;
using HomeSmart12.Entities;
using System.Threading.Tasks;

namespace HomeSmart12.Mqtt
{
    public partial class MqttManage
    {
        public IMqttClient mqttClient;
        public string generalTopic = App.homeUrl; //V1/currentTemp";

        public async Task Disconnet()
        {
            
            //SendUnSubscriptions();
            await mqttClient.DisconnectAsync();
     
        }

        public async Task Connect()
        {
            MqttConfiguration mqttConfiguration = new MqttConfiguration();
            mqttClient = await MqttClient.CreateAsync("mqtt.dioty.co", 1883); //pon el server
            var clientId = Guid.NewGuid().ToString().Replace("-", "");
            try
            {
                await mqttClient.ConnectAsync(new MqttClientCredentials(clientId, "jalemanyr@gmail.com", "9ee3ff4e"));
            }
            catch (Exception ex)
            {
                
            }
            
            //  

            try
            {
                mqttClient.MessageStream.Subscribe(async msg =>
               {
                   //All the messages from the Broker to any subscribed topic will get here
                   //The MessageStream is an Rx Observable, so you can filter the messages by topic with Linq to Rx
                   //The message object has Topic and Payload properties. The Payload is a byte[] that you need to deserialize 
                   //depending on the type of the message
                   //  var value = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                  await ProceesMessages(msg);
                   // Console.WriteLine($"Message received in topic {msg.Topic}");
               });
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            // Task.Delay(5000).Wait();
            SendSubscriptions();
        }

        async void MySubscription(String topic, MqttQualityOfService qos)
        {
            await mqttClient.SubscribeAsync(topic, qos);
        }

        public void SendSubscriptions()
        {

            foreach (var room in App.myRooms)
            {
                foreach (var device in room.Devices)
                {
                    device.Touched = false;
                    DeviceSubscription(device);
                    device.Touched = true;
                    // Task.Delay(100).Wait();
                }
            }

        }

        void DeviceSubscription(Device device)
        {
            var type = device.DeviceType;
            string topic = App.homeUrl + device.DeviceRoom + "/" + device.DeviceLetter + device.DeviceId + "/";
            switch (type)
            {
                case 0:
                    //Vent vent = (Vent)device;

                    MySubscription(topic + "currentTemp", 0);
                    MySubscription(topic + "ductTemp", 0);
                    MySubscription(topic + "damperStatus", 0);
                    MySubscription(topic + "setpointTemp", 0);
                    PublishMessage(topic + "refresh", "1");
                    break;
                case 1:
                    // Curtain curtain = (Curtain)device;
                    break;
                case 2:
                    // Light light = (Light)device;
                    MySubscription(topic + "switchStatus", 0);
                    PublishMessage(topic + "refresh", "1");
                    break;
                case 3:
                    // Extractor extractor = (Extractor)device;
                    break;
                case 4:
                    //Humidifier humidifier = (Humidifier)device;
                    MySubscription(topic + "currentHumidity", 0);
                    MySubscription(topic + "setpointHumidity", 0);
                    MySubscription(topic + "damperOpening", 0);
                    MySubscription(topic + "autoMode", 0);
                    PublishMessage(topic + "refresh", "1");
                    break;
            }
        }

        async void MyUnSubscription(String topic, MqttQualityOfService qos)
        {
            await mqttClient.UnsubscribeAsync(topic);
        }

        public void SendUnSubscriptions()
        {

            foreach (var room in App.myRooms)
            {
                foreach (var device in room.Devices)
                {
                    device.Touched = false;
                    DeviceUnSubscription(device);
                    device.Touched = true;
                    // Task.Delay(100).Wait();
                }
            }

        }

        void DeviceUnSubscription(Device device)
        {
            var type = device.DeviceType;
            string topic = App.homeUrl + device.DeviceRoom + "/" + device.DeviceLetter + device.DeviceId + "/";
            switch (type)
            {
                case 0:
                    //Vent vent = (Vent)device;

                    MyUnSubscription(topic + "currentTemp", 0);
                    MyUnSubscription(topic + "ductTemp", 0);
                    MyUnSubscription(topic + "damperStatus", 0);
                    MyUnSubscription(topic + "setpointTemp", 0);
                    
                    break;
                case 1:
                    // Curtain curtain = (Curtain)device;
                    break;
                case 2:
                    // Light light = (Light)device;
                    MyUnSubscription(topic + "switchStatus", 0);
                    
                    break;
                case 3:
                    // Extractor extractor = (Extractor)device;
                    break;
                case 4:
                    //Humidifier humidifier = (Humidifier)device;
                    MyUnSubscription(topic + "currentHumidity", 0);
                    MyUnSubscription(topic + "setpointHumidity", 0);
                    MyUnSubscription(topic + "damperOpening", 0);
                    MyUnSubscription(topic + "autoMode", 0);
                   
                    break;
            }
        }


        public async Task  ProceesMessages(MqttApplicationMessage msg)
        {
            var myTopic = msg.Topic;
            int devideId;
            //  int roomIndex;
            var found = false;
            int roomNumber = 0;

            var index = myTopic.IndexOf("/R", StringComparison.Ordinal);
            myTopic = myTopic.Substring(index + 2);
            var topic = myTopic;
            index = myTopic.IndexOf("/", StringComparison.Ordinal);
            roomNumber = int.Parse(myTopic.Substring(0, index));
            var deviceLetter = myTopic.Substring(index + 1, 1);
            topic = topic.Substring(index + 2);
            //    roomIndex = Integer.parseInt(myTopic.substring(0, index));
            index = topic.IndexOf("/", StringComparison.Ordinal);
            devideId = int.Parse(topic.Substring(0, index));

            var pay = double.Parse(Encoding.UTF8.GetString(msg.Payload));

            //var t = App.myRooms[roomNumber].Devices[devideId];

            foreach (var t in App.myRooms[roomNumber].Devices)
            {

                if (deviceLetter == t.DeviceLetter && devideId == t.DeviceId)
                {
                    t.Touched = false;
                    switch (t.DeviceType)
                    {
                        case 0:
                            { 
                                //Ventilation
                                found = true;
                                var vent = (Vent)t;

                                if (topic.Contains("currentTemp"))
                                    vent.CurrentTempView = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                if (topic.Contains("ductTemp"))
                                    vent.DuctTempView = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                if (topic.Contains("setpointTemp"))
                                {
                                    vent.SetpointTemp = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                    vent.OldSetpointTemp = vent.SetpointTemp;
                                    vent.SetpointTempView = vent.SetpointTemp;
                                    //vent.SetpointTemp = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                    MqttDelay();
                                }
                                if (topic.Contains("damperStatus"))
                                    vent.DamperStatusView = GetBooleanValue(Encoding.UTF8.GetString(msg.Payload));


                                //                        if (topic.contains("scanWallInfo")) {
                                //                            Toast.makeText(myContext, "Found New unit", Toast.LENGTH_SHORT).show();
                                //                            //vent.setDamperStatus(Boolean.parseBoolean(message.toString()));
                                //                        }
                                t.Touched = true;
                               // return found;
                                break;
                            }
                        case 1:
                            {
                                t.Touched = true;
                                break;
                            }
                        case 2:
                            {
                                //Light Switch
                                found = true;
                                var light = (Light)t;
                                if (topic.Contains("switchStatus"))
                                {
                                    light.StatusView = GetBooleanValue(Encoding.UTF8.GetString(msg.Payload));
                                    MqttDelay();
                                }
                                t.Touched = true;
                                //return found;
                                break;
                            }
                        case 3:
                            {
                                //Extractor
                                t.Touched = true;
                                break;
                            }
                        case 4:
                            {
                                //Humidifier
                                found = true;
                                var humidifier = (Humidifier)t;
                                // humidifier.Touched = false;
                                if (topic.Contains("currentHumidity"))
                                    humidifier.CurrentHumidityView = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                if (topic.Contains("setpointHumidity"))
                                {
                                    humidifier.SetpointHumidity = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                    humidifier.OldSetpointHumidity= humidifier.SetpointHumidity;
                                    humidifier.SetpointHumidityView = humidifier.SetpointHumidity;
                                    MqttDelay();
                                }
                                if (topic.Contains("damperOpening"))
                                    humidifier.DamperOpeningView = double.Parse(Encoding.UTF8.GetString(msg.Payload));
                                if (topic.Contains("autoMode"))
                                    humidifier.AutoModeView = GetBooleanValue(Encoding.UTF8.GetString(msg.Payload));
                                // humidifier.Touched = true;
                                t.Touched = true;
                               // return found;
                                break;

                            }

                    }

                }
            }
           // return found;
        }

        private bool GetBooleanValue(string value)
        {
            return ("Y".Equals(value.ToUpper())
                    || "1".Equals(value.ToUpper())
                    || "TRUE".Equals(value.ToUpper())
                    || "ON".Equals(value.ToUpper())
            );
        }

        void MqttDelay()
        {
             // Task.Delay(50).Wait();
        }

        public void PublishMessage(String topic, String value)
        {
            try
            {
                var mess = new MqttApplicationMessage(topic, Encoding.UTF8.GetBytes(value));
                //var mess = new MqttApplicationMessage(topic, new byte[]{} );
                //mess.Payload[0] = new byte[0]{};
                mqttClient.PublishAsync(mess, 0, false);

            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }
    }
}
