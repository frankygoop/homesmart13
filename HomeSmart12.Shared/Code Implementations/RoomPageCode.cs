﻿using System.Collections.Generic;

using Xamarin.Forms;
using System;

namespace HomeSmart12.Code_Implementations
{
    public class RoomPageCode : ContentPage
    {
        public RoomPageCode(int roomNumber)
        {
            var additionalOptions = new MenuItem { Text = "Options" };
            additionalOptions.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
            additionalOptions.Clicked += (sender, e) =>
            {
                var selectedItem = (MenuItem)sender;
                var selectedProduct = selectedItem.CommandParameter as Entities.Device;
                DisplayAlert("Options", "", "OK");
            };

            // Padding = new Thickness(0, 100, 0, 10);
            var image = new Image { };
            ListView listView = new ListView(ListViewCachingStrategy.RecycleElement)
            {
                HasUnevenRows = true,
                SelectedItem = "OnItemSelected"
            };

            // listView.IsPullToRefreshEnabled = true;

            image.Source = App.myRooms[roomNumber].ImageName;
            image.WidthRequest = 300;
            image.HeightRequest = 200;

            listView.ItemsSource = App.myRooms[roomNumber].Devices;
            var cellTemplate = new MyTemplateSelector(App.myRooms[roomNumber].Devices, roomNumber, 3);

            ViewCell viewCell = new ViewCell();
            viewCell.ContextActions.Add(additionalOptions);

            // cellTemplate.Bindings()
            listView.ItemTemplate = cellTemplate;

            //listView.ItemSelected += (e, s) =>
            //{
            //    DisplayAlert(" Item selected ", (s.SelectedItem as Entities.Device).DeviceName, "ok");
            //};

            //listView.ItemTapped += (e, s) =>
            //{
            //    e = null;
            //    //DisplayAlert(" ItemTapped ", (s.Item as Entities.Device).DeviceName, "ok");
            //    //if ((s.Item as Entities.Device).DeviceType == 4)
            //    //{
            //    //    DisplayAlert(" item is Humidifier ", (s.Item as Entities.Humidifier).DamperOpening.ToString(), "ok");
            //    //}
            //};

            var stackLayout = new StackLayout
            {
               // Padding = new Thickness(5, 5, 5, 5),
                VerticalOptions = LayoutOptions.Fill,
                Orientation = StackOrientation.Vertical,
                //HorizontalOptions=LayoutOptions.StartAndExpand
            };
            
            stackLayout.Children.Add(image);
            stackLayout.Children.Add(listView);
            ScrollView scrollView = new ScrollView { Content = stackLayout };
            Content = scrollView;
        }

        void OnItemSelected(Object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var selection = e.SelectedItem as Entities.Device;
                DisplayAlert(" selected ", selection.DeviceName, "ok");
                #region DisableSelectionHighLighting
                ((ListView)sender).SelectedItem = null;
                #endregion
            }
        }
    }

    public class MyTemplateSelector : DataTemplateSelector
    {
        public DataTemplate VentTemplate = new DataTemplate(typeof(VentLayout));
        public DataTemplate CurtainTemplate = new DataTemplate(typeof(CurtainLayout));
        public DataTemplate HumidifierTemplate = new DataTemplate(typeof(HumidifierLayout));
        public DataTemplate LightTemplate = new DataTemplate(typeof(LightLayout));
        public DataTemplate ExtractorTemplate = new DataTemplate(typeof(ExtractorLayout));
        private List<Entities.Device> devices;

        public MyTemplateSelector(List<Entities.Device> devices, int rN, int pos)
        {
            this.devices = devices;
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            switch (((Entities.Device)item).DeviceType)
            {
                case 1: return CurtainTemplate;
                case 2: return LightTemplate;
                case 4: return HumidifierTemplate;
                case 3: return ExtractorTemplate;
                default: return VentTemplate;
            }
        }
    }

   
}