﻿
using HomeSmart12.Entites;
using HomeSmart12.Mqtt;
using System.Collections.Generic;


using Xamarin.Forms;



/*
 
Vent            - 0 - "V"
Curtain         - 1 - "C"
Light           - 2 - "L"
Extractor       - 3 - "E"
Humidifier      - 4 - "H"
Dimmer          - 5 - "D"  
     
 */



namespace HomeSmart12
{
    public partial class App : Application
    {
        public static List<Room> myRooms = new List<Room>();
        public static int selectedRoom;
        public static MqttManage myMqttClient;
        public static  string homeUrl="/jalemanyr@gmail.com/feeds/R";

        public App()
        {
            //InitializeComponent();
          //  ActivityIndicator activityIndicator = new ActivityIndicator { IsRunning = true };

            AddElements();
            myMqttClient = new MqttManage();
            //myMqttClient.Connect();

            MainPage = new NavigationPage(new MainTabContainer());
            
           
            //activityIndicator.IsRunning = false;

        }

        protected override void OnStart()
        {
            myMqttClient.Connect();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            myMqttClient.Disconnet();
            // Handle when your app sleeps
        }

        
        protected override void OnResume()
        {
            myMqttClient.Connect();
            // Handle when your app resumes
        }

        void AddElements()
        {
            myRooms.Clear();

            var roomNumber = 0;
            myRooms.Add(new Room(roomNumber, 0, "living"));
            myRooms[roomNumber].Devices.Add(new Entities.Vent(roomNumber, 0, 22, 22));
            myRooms[roomNumber].Devices.Add(new Entities.Light(roomNumber, 0, false, "Office"));
            myRooms[roomNumber].Devices.Add(new Entities.Light(roomNumber, 1, false, "Extractor"));
            // myRooms[roomNumber].Devices.Add(new Entities.Light(roomNumber, 1, false, "Extra"));
            myRooms[roomNumber].Devices.Add(new Entities.Curtain(roomNumber, 0, 20));
            myRooms[roomNumber].Devices.Add(new Entities.Humidifier(roomNumber, 0, 40, 90, "MainHum"));

            roomNumber = 1;
            myRooms.Add(new Room(roomNumber, 0, "bedroom"));
            myRooms[roomNumber].Devices.Add(new Entities.Vent(roomNumber, 0, 21, 21));
            myRooms[roomNumber].Devices.Add(new Entities.Humidifier(roomNumber, 0, 40, 90, "MainHum"));

            //myRooms[roomNumber].Devices.Add(new Entities.Curtain(roomNumber, 1, 10));

            roomNumber = 2;
            myRooms.Add(new Room(roomNumber, 0, "office"));
            myRooms[roomNumber].Devices.Add(new Entities.Vent(roomNumber, 0, 21, 21));
            // myRooms[roomNumber].Devices.Add(new Entities.Curtain(roomNumber, 2, 10));

            //roomNumber = 3;
            //myRooms.Add(new Room(roomNumber, 0, "Extra"));
            //myRooms[roomNumber].Devices.Add(new Entities.Vent(roomNumber, 0, 25, 22));

        }

    }
}
