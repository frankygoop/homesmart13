﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HomeSmart12.Code_Implementations;

namespace HomeSmart12
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabContainer : TabbedPage
    {
        public MainTabContainer()
        {
            InitializeComponent();
            CreateRoomsTabs();

            //var i = 0;
            //this.CurrentPageChanged += (object sender, EventArgs e) => {
            //    i = this.Children.IndexOf(this.CurrentPage);
            //    App.selectedRoom = i;
            //    System.Diagnostics.Debug.WriteLine("Page No:" + i);
            //};

           

        }



        void CreateRoomsTabs()
        {
            for (int i = 0; i < App.myRooms.Count; i++)
            {
               
                Children.Add(new RoomPageCode(i) { Title = App.myRooms[i].Name });
            }

        }
    }
}