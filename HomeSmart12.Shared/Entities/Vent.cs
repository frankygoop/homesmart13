﻿//using Java.IO;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace HomeSmart12.Entities
{
    public class Vent : Device, INotifyPropertyChanged
    {
        public int Type { get; set; }
        public String Topic { get; set; }
        public double CurrentTemp { get; set; }
        public double SetpointTemp { get; set; }
        public double DuctTemp { get; set; }
        public bool DamperStatus { get; set; }
       // public bool Touched { get; set; }
        // public bool CommStatus { get; set; }
        public double OldSetpointTemp { get; set; }
        public string ImageName { get; set; }
        
        public Vent(int Room, int Id, double SetpointTemp, double CurrentTemp)
            : base(Room, Id, "V" + Id)
        {
            this.CurrentTemp = CurrentTemp;
            this.SetpointTemp = SetpointTemp;
            OldSetpointTemp = SetpointTemp;
            this.DuctTemp = 10;
            Type = 0;
            DeviceType = Type;
            DeviceRoom = Room;
            //DeviceName = "";
            DeviceId = Id;
           // Touched = false;
            DeviceLetter = "V";
            ImageName = "ventopen.png";
        }

        public double CurrentTempView
        {
            get { return CurrentTemp; }
            set
            {
                CurrentTemp = value;
                OnPropertyChanged();
            }
        }


        public double SetpointTempView
        {
            get { return SetpointTemp; }
            set
            {
               
                if (Touched)
                    SetpointTemp = Math.Round(value, 1);
                {
                    if (OldSetpointTemp != SetpointTemp)
                    {
                        string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                        App.myMqttClient.PublishMessage(topic + "changeSetpointTemp", SetpointTemp.ToString());
                        OldSetpointTemp = SetpointTemp;
                    }
                }
                OnPropertyChanged();
            }
        }

        public double DuctTempView
        {
            get => DuctTemp;
            set
            {
                DuctTemp = value;
                OnPropertyChanged();
            }
        }


        public string ImageNameView => DamperStatus ? "ventopen" : "ventclose";

        public bool DamperStatusView
        {
            get => DamperStatus;
            set
            {
                DamperStatus = value;
                OnPropertyChanged();
                OnPropertyChanged("ImageNameView");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }


}



