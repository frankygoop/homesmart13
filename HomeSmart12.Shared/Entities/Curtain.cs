﻿//using Java.IO;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HomeSmart12.Entities
{
    public class Curtain : Device, INotifyPropertyChanged

    {
        public int Type { get; private set; }
        public string Topic { get; set; }
        public int Opening { get; set; }
        public bool CommStatus { get; set; }
        public string ImageName { get; set; }
        public int OldOpening { get; set; }
        
        public Curtain(int room, int id, int opening)
            : base(room, id, "C" + id)
        {
            this.Opening = opening;
            Type = 1;
            DeviceType = Type;
            DeviceRoom = room;
            DeviceLetter = "C";
        }

        public int OpeningView
        {
            get => Opening;
            set
            {
                Opening = value;
                //if (Touched)
                //{
                //    if (OldOpening != Opening)
                //    {
                //        string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                //        App.myMqttClient.PublishMessage(topic + "opening", Opening.ToString());
                //        OldOpening = Opening;
                //    }
                //}
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
