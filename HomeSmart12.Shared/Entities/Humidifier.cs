﻿//using Java.IO;
using System.ComponentModel;
using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace HomeSmart12.Entities
{
    public class Humidifier : Device, INotifyPropertyChanged
    {
        // public int Type { get; set; }
        public string Topic { get; set; }
        public double CurrentHumidity { get; set; }
        public double SetpointHumidity { get; set; }
        public double DamperOpening { get; set; }
        public bool AutoMode { get; set; }
        public double OldSetpointHumidity { get; set; }
        public string ImageName { get; set; }
        public bool SetpointEnable { get; set; }

        public Humidifier(int room, int id, double setpointHumidity, double damperOpening, string name)
            : base(room, id, name)
        {
            this.SetpointHumidity = setpointHumidity;
            OldSetpointHumidity = setpointHumidity;
            this.DamperOpening = damperOpening;
            //Type = 4;
            Topic = "";
            AutoMode = false;
            CurrentHumidity = 40;
            DeviceType = 4;
            DeviceRoom = room;
            DeviceLetter = "H";
            SetpointEnable = false;
        }


        public double CurrentHumidityView
        {
            get { return CurrentHumidity; }
            set
            {
                CurrentHumidity = value;
                OnPropertyChanged();
            }
        }

        public string ImageNameView
        {
            get
            {
               
                return ImageName;
            }
            set
            {
                ImageName = value;
                OnPropertyChanged();
            }

        }
        public double DamperOpeningView
        {
            get { return DamperOpening * 100 / 90; }
            set
            {
                DamperOpening = value;
                if (DamperOpening == 0)
                {
                    ImageName = "humidifieroff1";
                }

                else
                {
                    ImageName = "humidifieron1";
                }
                OnPropertyChanged();
                OnPropertyChanged("ImageNameView");
            }
        }

        public double SetpointHumidityView
        {
            get { return SetpointHumidity; }
            set
            {
               
                if (SetpointEnableView)
                {
                    string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                    if (Touched)
                    {
                        SetpointHumidity = Math.Round(value, 0);
                        try
                        {
                            if (OldSetpointHumidity != SetpointHumidity)
                            {
                                   App.myMqttClient.PublishMessage(topic + "changeSetpointHumidity",SetpointHumidity.ToString());
                                OldSetpointHumidity = SetpointHumidity;
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                 }
                OnPropertyChanged();
            }
        }

        public bool AutoModeView
        {
            get { return AutoMode; }
            set
            {
                AutoMode = value;

                if (Touched)
                {
                    var flag = Convert.ToInt32(AutoMode);
                    string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                    App.myMqttClient.PublishMessage(topic + "autoMode", flag.ToString());
                }
                OnPropertyChanged();
                OnPropertyChanged("TextAutoModeView");
                OnPropertyChanged("SetpointEnableView");
            }
        }

        public bool SetpointEnableView => !AutoMode;

        public String TextAutoModeView => AutoMode ? "AUTO " : "MANUAL";
        
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public static implicit operator Humidifier(Element v)
        {
            throw new NotImplementedException();
        }
    }


}
