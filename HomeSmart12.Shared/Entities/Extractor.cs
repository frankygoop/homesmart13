﻿//using Java.IO;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;


namespace HomeSmart12.Entities
{
    public class Extractor : Device, INotifyPropertyChanged
    {
        private int Type = 3;
        private String Topic { get; set; }
        private bool AutoMode { get; set; }
        private bool Status { get; set; }

        public Extractor(int room, int id, bool AutoMode, bool status, string name)
            :base(room,id,  name)
        {
            this.AutoMode = AutoMode;
            this.Status = Status;
           
            Type = 3;
            DeviceType=Type;
            DeviceRoom=room;
            DeviceLetter = "E";
        }

        public bool StatusView
        {
            get => Status;
            set
            {
                Status = value;
                if (Touched)
                {
                    string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                    var flag = Convert.ToInt32(Status);
                    App.myMqttClient.PublishMessage(topic + "changeStatus", flag.ToString());
                }

                OnPropertyChanged();
                OnPropertyChanged("TextStatusView");
                OnPropertyChanged("ImageNameView");
            }
        }


        public string ImageNameView => Status ? "lighton1" : "lightoff1";

        public string TextStatusView => Status ? "ON " : "OFF";


        public new event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }

        public new void Dispose()
        {
            throw new NotImplementedException();
        }

       

    }
}
