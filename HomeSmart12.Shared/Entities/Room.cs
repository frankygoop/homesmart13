﻿//using Java.IO;
using System;
using System.Collections.Generic;

using HomeSmart12.Entities;

namespace HomeSmart12.Entites
{
    //[Serializable]
    public class Room 
    {
        public int Id { get; set; }
        public int IconId { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public List<Device> Devices { get; set; }


        public IntPtr Handle => throw new NotImplementedException();
   
        public Room(int Id, int IconId, string Name)
        {

            Devices = new List<Device> { };
            this.Id = Id;
            this.IconId = IconId;
            this.Name = Name;
            ImageName = Name;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
