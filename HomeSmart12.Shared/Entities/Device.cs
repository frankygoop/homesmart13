﻿//using Java.IO;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HomeSmart12.Entities
{
    public class Device: INotifyPropertyChanged
    {

        public int DeviceId { get; set; }
        public  string DeviceLetter { get; set; }
        public string DeviceName { get; set; }
        public int DeviceType { get; set; }
        public int DeviceRoom { get; set; }
        public bool Touched { get; set; }
        public IntPtr Handle => throw new NotImplementedException();

        public Device(int Room, int Id, string Name)
        {
            DeviceRoom = Room;
            DeviceId = Id;
            DeviceName = Name;
            DeviceType = -1;
            Touched = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


    }
}
