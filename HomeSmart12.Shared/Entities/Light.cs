﻿//using Java.IO;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HomeSmart12.Entities
{
    public class Light : Device, INotifyPropertyChanged

    {
        public int Type { get; private set; }
        public string Topic { get; set; }
        public bool Status { get; set; }
        public bool CommStatus { get; set; }
        public string ImageName { get; set; }
       
        public Light(int room, int id, bool status, string name)
            : base(room, id, name)
        {
            this.Status = status;
            Type = 2;
            DeviceType = Type;
            DeviceRoom = room;
            
           DeviceLetter = "L";
        }

        public bool StatusView
        {
            get => Status;
            set
            {
                Status = value;
                if (Touched)
                {
                    string topic = App.homeUrl + DeviceRoom + "/" + DeviceLetter + DeviceId + "/";
                    var flag = Convert.ToInt32(Status);
                    App.myMqttClient.PublishMessage(topic + "changeStatus", flag.ToString());
                 }
             
                OnPropertyChanged();
                OnPropertyChanged("TextStatusView");
                OnPropertyChanged("ImageNameView");
            }
        }

        public string ImageNameView => Status ? "lighton1" : "lightoff1";

        public string TextStatusView => Status ? "ON " : "OFF";

        public new event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string nameOfProperty = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameOfProperty));
        }

        public new void Dispose()
        {
            throw new NotImplementedException();
        }

       
    }
}
